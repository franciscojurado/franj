package Main;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;

import org.dreambot.api.methods.Calculations;
import org.dreambot.api.methods.map.Area;
import org.dreambot.api.methods.map.Tile;
import org.dreambot.api.methods.skills.Skill;
import org.dreambot.api.script.AbstractScript;
import org.dreambot.api.script.Category;
import org.dreambot.api.script.ScriptManifest;
import org.dreambot.api.utilities.Timer;
import org.dreambot.api.utilities.impl.Condition;
import org.dreambot.api.wrappers.interactive.Entity;
import org.dreambot.api.wrappers.interactive.GameObject;
import org.dreambot.api.wrappers.interactive.NPC;

@ScriptManifest(name = "Franjey's Ess Miner", author = "Franjey", version = 1.01, description = "Mines rune ess", category = Category.MINING)


public class EssenceMiner extends AbstractScript{

	private final Color color1 = new Color(51, 51, 51, 147);
	private final Color color2 = new Color(138, 54, 15);
	private final Color color3 = new Color(255, 255, 255);
	private final BasicStroke stroke1 = new BasicStroke(5);
	private final Font font1 = new Font("Arial", Font.BOLD, 13);
	private final Font font2 = new Font("Arial", Font.BOLD, 0);
	private final Font font3 = new Font("Arial", 0, 13);
	private Timer t = new Timer();

	private Area portalArea = new Area(3252, 3404, 3256, 3399);
	private Area generalArea = new Area(3248, 3428, 3268, 3392);
	private int oreMined;

	@Override
	public void onStart() {
		getSkillTracker().start(Skill.MINING);
		getSkillTracker().start();
	}

	@Override
	public int onLoop() {

		Entity door = getGameObjects().closest(object -> object.hasAction("Open") && (object.getTile().equals(new Tile(3253, 3398, 0)) || object.getTile().equals(new Tile(325, 3400, 0))) );

		getDialogues().clickContinue();

		if(!getWalking().isRunEnabled() && getWalking().getRunEnergy() > Calculations.random(30,50)){
			getWalking().toggleRun();
		}

		////String[] pickaxe = new String[]{"Bronze", "Iron", "Steel"...};
		String[] pickaxe = new String[6];
		pickaxe[0] = "Bronze pickaxe";
		pickaxe[1] = "Iron pickaxe";
		pickaxe[2] = "Steel pickaxe";
		pickaxe[3] = "Mithril pickaxe";
		pickaxe[4] = "Adamant pickaxe";
		pickaxe[5] = "Rune pickaxe";

		if(generalArea.contains(getPlayers().myPlayer().getTile())) {
			if(getInventory().isFull()) { 
				if (!getBank().isOpen()) {
					log("Walking to the bank");
					getBank().openClosest();
					sleepUntil(new Condition() {
						public boolean verify() {
							return getBank().isOpen();
						}
					}, Calculations.random(1600, 1800));
				} else {
					getBank().depositAllExcept(pickaxe);
					log("Depositing rune ess");
					sleepUntil(new Condition() {
						public boolean verify() {
							return getInventory().isFull();
						}
					}, Calculations.random(1600, 1800));
				}
			}
			else{
				if(getInventory().onlyContains(pickaxe) || getEquipment().contains(pickaxe)) {
					if(!portalArea.contains(getPlayers().myPlayer().getTile())) {
						///Walk to a random tile inside the portal area
						Tile[] tiles = portalArea.getTiles();
						Tile randomTile = tiles[Calculations.random(0, tiles.length - 1)];
						if (randomTile != null) {
							log("Walking to the portal");
							getWalking().walk(randomTile);
							sleepUntil(new Condition() {
								public boolean verify() {
									return portalArea.contains(getPlayers().myPlayer()
											.getTile());
								}
							}, Calculations.random(1600, 1800));
						}
					}
					else if(door instanceof GameObject){
						Tile t = new Tile(3253, 3398, 0);
						if(t != null) {
							if(door.hasAction("Open")) {
								log("Opening door");
								door.interact("Open");
							}
						}	
					}
					else{
						NPC npc = getNpcs().closest("Aubury");
						if(npc != null) {
							log("Teleporting");
							npc.interact("Teleport");

							sleepUntil(new Condition() {
								@Override
								public boolean verify() {
									return !portalArea.contains(getPlayers().myPlayer().getTile());
								}
							}, Calculations.random(20000, 30000));
						}
					}
				}
			}	
		}

		else {
			GameObject essRock = getGameObjects().closest("Rune Essence");
			if(essRock != null) {
				if(!getInventory().isFull()) {
					if(essRock.distance(getPlayers().myPlayer().getTile()) < 10) {
						log("Mining");
						essRock.interact("Mine");

						sleepUntil(new Condition() {
							@Override
							public boolean verify() {
								return getInventory().isFull();
							}

						}, Calculations.random(20000, 30000));

					}
					else {
						getWalking().walk(essRock);
					}
				}
				else{
					//Handling the portal - Its a dynamic entity. Debug tool - Portal treated as NPC & GameObject
					Entity portal = getGameObjects().closest(object -> object.hasAction("Use") || object.hasAction("Exit"));
					if (portal == null) {
						portal = getNpcs().closest(object -> object.hasAction("Use") || object.hasAction("Exit"));
					}

					if(portal != null) {
						if(portal instanceof NPC){
							Tile t = portal.getTile();
							if(t != null) {
								if(portal.hasAction("Use")) {
									portal.interact("Use");
								}
								else {
									portal.interact("Exit");
								}	
							}	
						}
						else{
							if(portal instanceof GameObject){
								Tile t = portal.getTile();
								if(t != null) {
									if(portal.hasAction("Use")) {
										portal.interact("Use");

										sleepUntil(new Condition() {
											@Override
											public boolean verify() {
												return portalArea.contains(getPlayers().myPlayer().getTile());
											}
										}, Calculations.random(20000, 30000));
									}
									else {
										portal.interact("Exit");

										sleepUntil(new Condition() {
											@Override
											public boolean verify() {
												return portalArea.contains(getPlayers().myPlayer().getTile());
											}
										}, Calculations.random(20000, 30000));
									}	
								}
							}
						}
					}
				}
			}

		}
		return Calculations.random(300, 600);
	}


	/*
	 * Paint
	 */
	public long getGainedExperience(Skill firemaking){         //gained experience
		long fm;
		fm = getSkillTracker().getGainedExperience(Skill.MINING);
		return fm;
	}

	public long getGainedExperiencePerHour(Skill firemaking){  //gained experience per hour 
		long fm;
		fm = getSkillTracker().getGainedExperiencePerHour(Skill.MINING);
		return fm;
	}

	public long getStartLevel(){  //Start level
		long fm;
		fm = getSkillTracker().getStartLevel(Skill.MINING);
		return fm;
	}

	public long getGainedLevels(){  //gained levels 
		long fm;
		fm = getSkillTracker().getGainedLevels(Skill.MINING);
		return fm;
	}

	public long getExperienceToLevel(){  //Exp to level 
		long fm;
		fm = getSkills().experienceToLevel(Skill.MINING);
		return fm;
	}

	public long getTimeToLevel() {
		long fmxp;
		long xphr;
		fmxp = getExperienceToLevel();
		xphr = getGainedExperiencePerHour(Skill.MINING);
		return fmxp/xphr;
	}

	public void onPaint(Graphics g1) {
		oreMined = (int)Math.floor(getSkillTracker().getGainedExperience(Skill.MINING)/5); //Detects mined essence/ores

		if(t == null){
			t = new Timer(0);
		}
		Graphics2D g = (Graphics2D)g1;
		Stroke stroke = g.getStroke();
		g.setColor(color1);
		g.fillRect(3, 4, 205, 160);
		g.setColor(color2);
		g.setStroke(stroke1);
		g.drawRect(3, 4, 205, 160);
		g.setFont(font1);
		g.setColor(color3);
		g.drawString(getManifest().name() + "         " + "v" + getManifest().version(), 12, 29);
		g.setFont(font2);
		g.setFont(font3);
		g.drawString("Time running: " + Timer.formatTime(t.elapsed()), 12, 59);
		g.drawString("XP gained: " + getGainedExperience(Skill.MINING) + "(" + getGainedExperiencePerHour(Skill.MINING) + ")", 12, 99);
		g.drawString("Levels gained: " + getSkills().getRealLevel(Skill.MINING) + "(+" + getGainedLevels() + ")", 12, 79);
		g.drawString("XP to level: " + getExperienceToLevel(), 12, 121);
		g.drawString("Mined [P/H]: " + oreMined + " [" + t.getHourlyRate(oreMined) + "]", 12, 141);
		g.setStroke(stroke);
	}
}