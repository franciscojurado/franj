package main;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;

import org.dreambot.api.methods.map.Area;
import org.dreambot.api.methods.map.Tile;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class gui extends JFrame {

	private Area burnArea;
	private Tile startTile;
	private Tile startTileWalk;
	private Tile newFirstStartTile;
	private Tile newSecondStartTile;
	private Tile switchTile;
	private Tile switchTileNew;
	private String logsName;
	public boolean start;

	public String getLogsName() {
		return logsName;
	}

	public Area getBurnArea() {
		return burnArea;
	}
	
	public Tile getStartTile() {
		return startTile;
	}
	
	public Tile getStartTileWalk() {
		return startTileWalk;
	}
	
	public Tile getNewFirstStartTile() {
		return newFirstStartTile;
	}
	
	public Tile getNewSecondStartTile() {
		return newSecondStartTile;
	}
	
	public Tile getSwitchTile() {
		return switchTile;
	}
	
	public Tile getSwitchTileNew() {
		return switchTileNew;
	}

	/**
    *
    */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					gui frame = new gui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public gui() {
		setTitle("Franjey AIO FireMaking");
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 298, 305);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblSelectTheArea2 = new JLabel(
				"Insert the name of logs to be burnt:");
		lblSelectTheArea2.setBounds(10, 11, 414, 14);
		contentPane.add(lblSelectTheArea2);

		final JList<fireMakingEnums> list = new JList<fireMakingEnums>(
				fireMakingEnums.values());
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.setBounds(20, 94, 178, 145);

		JTextField jtbWriteLogsName = new JTextField();
		jtbWriteLogsName.setText("Logs");
		jtbWriteLogsName.setBounds(10, 26, 214, 17);
		contentPane.add(jtbWriteLogsName);

		JButton btnStart = new JButton("Start Script");
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				burnArea = list.getSelectedValue().getBurnArea();
				startTile = list.getSelectedValue().getStartTile();
				startTileWalk = list.getSelectedValue().getStartTileWalk();
				newFirstStartTile = list.getSelectedValue().getNewFirstStartTile();
				newSecondStartTile = list.getSelectedValue().getNewSecondStartTile();
				switchTile = list.getSelectedValue().getSwitchTile();
				switchTileNew = list.getSelectedValue().getSwitchTileNew();
				logsName = jtbWriteLogsName.getText();
				start = true;
				setVisible(false);
			}
		});
		btnStart.setBounds(69, 238, 99, 23);
		contentPane.add(btnStart);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 46, 214, 186);
		contentPane.add(scrollPane);
		scrollPane.setViewportView(list);
	}
}
