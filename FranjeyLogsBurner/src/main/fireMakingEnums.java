package main;

import org.dreambot.api.methods.map.Area;
import org.dreambot.api.methods.map.Tile;

public enum fireMakingEnums {

	/*
	 * Define burning areas
	 */
	EDGEVILLE(new Area(new Tile(3101, 3486, 0), new Tile(3100, 3486, 0), new Tile(3099, 3486, 0), new Tile(3098, 3486, 0),
					new Tile(3097, 3486, 0), new Tile(3096, 3486, 0), new Tile(3095, 3486, 0), new Tile(3094, 3486, 0),
					new Tile(3093, 3486, 0), new Tile(3092, 3486, 0), new Tile(3091, 3486, 0), new Tile(3090, 3486, 0),
					new Tile(3089, 3486, 0), new Tile(3088, 3486, 0), new Tile(3087, 3486, 0), new Tile(3086, 3486, 0),
					new Tile(3085, 3486, 0), new Tile(3084, 3486, 0), new Tile(3083, 3486, 0), new Tile(3082, 3486, 0),
					new Tile(3081, 3486, 0), new Tile(3080, 3486, 0), new Tile(3079, 3486, 0), new Tile(3078, 3486, 0),
					new Tile(3077, 3486, 0), new Tile(3076, 3486, 0), new Tile(3075, 3486, 0), new Tile(3074, 3486, 0)),
					new Tile(3101, 3486, 0), new Tile(3101, 3486, 0), new Tile(3077, 3486, 0), new Tile(3081,3485, 0), 
					new Tile(3077, 3485, 0), new Tile(3081,3484, 0)),
					
	FALADOR_EAST(new Area(new Tile(3032,3361,0), new Tile(3031,3361,0), new Tile(3030,3361,0), new Tile(3029,3361,0), new Tile(3028,3361,0),
			new Tile(3027,3361,0), new Tile(3026,3361,0), new Tile(3025,3361,0), new Tile(3024,3361,0), new Tile(3023,3361,0), 
			new Tile(3022,3361,0), new Tile(3021,3361,0), new Tile(3020,3361,0), new Tile(3019,3361,0), new Tile(3018,3361,0), new Tile(3017,3361,0), 
			new Tile(3016,3361,0), new Tile(3015,3361,0), new Tile(3014,3361,0), new Tile(3013,3361,0), new Tile(3012,3361,0), new Tile(3011,3361,0),
			new Tile(3010,3361,0), new Tile(3009,3361,0), new Tile(3008,3361,0), new Tile(3007,3361,0), new Tile(3006,3361,0), new Tile(3005,3361,0),
			new Tile(3004,3361,0)), new Tile(3032,3361,0), new Tile(3032,3361,0), new Tile(0000,0000,0), new Tile(0000,0000,0), new Tile(0000,0000,0),
			new Tile(0000,0000,0)),
			
	VARROCK_EAST(new Area(new Tile(3270, 3429, 0), new Tile(3269, 3429, 0), new Tile(3268, 3429, 0), new Tile(3267, 3429, 0),
					new Tile(3266, 3429, 0), new Tile(3265, 3429, 0), new Tile(3264, 3429, 0), new Tile(3263, 3429, 0), new Tile(3262, 3429, 0),
					new Tile(3261, 3429, 0), new Tile(3260, 3429, 0), new Tile(3259, 3429, 0), new Tile(3258, 3429, 0), new Tile(3257, 3429, 0),
					new Tile(3256, 3429, 0), new Tile(3255, 3429, 0), new Tile(3254, 3429, 0), new Tile(3253, 3429, 0), new Tile(3252, 3429, 0),
					new Tile(3251, 3429, 0), new Tile(3250, 3429, 0), new Tile(3249, 3429, 0), new Tile(3248, 3429, 0), new Tile(3247, 3429, 0),
					new Tile(3246, 3429, 0), new Tile(3245, 3429, 0), new Tile(3244, 3429, 0), new Tile(3243, 3429, 0)),
					new Tile(3270, 3429, 0), new Tile(3270, 3429, 0), new Tile(0000, 0000, 0), new Tile(0000, 0000, 0), new Tile(0000, 0000, 0), 
					new Tile(0000, 0000, 0)), 
					
	VARROCK_WEST(new Area(new Tile(3195, 3432, 0), new Tile(3194, 3432, 0), new Tile(3193, 3432, 0), new Tile(3192, 3432, 0), new Tile(3191, 3432, 0),
			new Tile(3190, 3432, 0), new Tile(3189, 3432, 0), new Tile(3188, 3432, 0), new Tile(3187, 3432, 0), new Tile(3186, 3432, 0), 
			new Tile(3185, 3432, 0), new Tile(3184, 3432, 0), new Tile(3183, 3432, 0), new Tile(3182, 3432, 0), new Tile(3181, 3432, 0),
			new Tile(3180, 3432, 0), new Tile(3179, 3432, 0), new Tile(3178, 3432, 0), new Tile(3177, 3432, 0), new Tile(3176, 3432, 0),
			new Tile(3175, 3432, 0), new Tile(3174, 3432, 0), new Tile(3173, 3432, 0), new Tile(3172, 3432, 0), new Tile(3171, 3432, 0),
			new Tile(3170, 3432, 0), new Tile(3169, 3432, 0), new Tile(3168, 3432, 0)),
					new Tile(3195, 3432, 0), new Tile(3195, 3432, 0), new Tile(3173, 3432, 0), new Tile(3176, 3433, 0), new Tile(0000, 0000, 0), 
					new Tile(0000, 0000, 0));


	private final Area burnArea; //define burning area
	private final Tile startTile; //define the first tile to start burning - Obtained through the WalkOnScreen()
	private final Tile startTileWalk; // define the first tile to start burning - Obtained through the Walk()
	private final Tile newFirstStartTile; //The first tile that the player will switch to, after reaching the switchTile 
	private final Tile newSecondStartTile; //The second tile that the player will switch to, after reaching the switchTileNew
	private final Tile switchTile; //This is the switching tile, for the player to move to the newFirstStartTile, to continue burning the logs
	private final Tile switchTileNew; //This is the switching tile, for the player to move to the newSecondStartTile, to finish burning remaining logs

	fireMakingEnums(Area burnArea, Tile startTile, Tile startTileWalk, Tile switchTile, Tile newFirstStartTile, Tile switchTileNew,
			Tile newSecondStartTile) {
		this.burnArea = burnArea;
		this.startTile = startTile;
		this.startTileWalk = startTileWalk;
		this.newFirstStartTile = newFirstStartTile;
		this.newSecondStartTile = newSecondStartTile;
		this.switchTile = switchTile;
		this.switchTileNew = switchTileNew;
	}

	public Area getBurnArea() {
		return burnArea;
	}

	public Tile getStartTile() {
		return startTile;
	}
	
	public Tile getStartTileWalk() {
		return startTileWalk;
	}

	public Tile getNewFirstStartTile() {
		return newFirstStartTile;
	}

	public Tile getNewSecondStartTile() {
		return newSecondStartTile;
	}

	public Tile getSwitchTile() {
		return switchTile;
	}
	
	public Tile getSwitchTileNew() {
		return switchTileNew;
	}
	
}

/*
 * Edgeville Area
 * (new Tile(3096, 3486, 0), new Tile(3095, 3486, 0),
					new Tile(3094, 3486, 0), new Tile(3093, 3486, 0), new Tile(
							3092, 3486, 0), new Tile(3091, 3486, 0), new Tile(
							3090, 3486, 0), new Tile(3089, 3486, 0), new Tile(
							3088, 3486, 0), new Tile(3087, 3486, 0), new Tile(
							3086, 3486, 0), new Tile(3085, 3486, 0), new Tile(
							3084, 3486, 0), new Tile(3083, 3486, 0), new Tile(
							3082, 3486, 0), new Tile(3081, 3486, 0), new Tile(
							3080, 3486, 0), new Tile(3079, 3486, 0), new Tile(
							3078, 3486, 0), new Tile(3096, 3485, 0), new Tile(
							3095, 3485, 0), new Tile(3094, 3485, 0), new Tile(
							3093, 3485, 0), new Tile(3092, 3485, 0), new Tile(
							3091, 3485, 0), new Tile(3090, 3485, 0), new Tile(
							3089, 3485, 0), new Tile(3088, 3485, 0), new Tile(
							3087, 3485, 0), new Tile(3086, 3485, 0), new Tile(
							3085, 3485, 0), new Tile(3084, 3485, 0))
							*/
