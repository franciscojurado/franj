package main;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Stroke;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;

import org.dreambot.api.script.Category;
import org.dreambot.api.script.ScriptManifest;
import org.dreambot.api.methods.Calculations;
import org.dreambot.api.methods.map.Area;
import org.dreambot.api.methods.map.Tile;
import org.dreambot.api.methods.skills.Skill;
import org.dreambot.api.methods.skills.Skills;
import org.dreambot.api.script.AbstractScript;
import org.dreambot.api.utilities.Timer;
import org.dreambot.api.utilities.impl.Condition;

@ScriptManifest(name = "Franjey AIO Firemaking", author = "Franjey", version = 1.02, description = "Burns different logs.", category = Category.FIREMAKING)

/* Author: Franjey
 * Date finished: 08/03/2015
 * 
 * Created using: Eclipse
 * 
 * Instructions: 
 * -If the bank contains less than 27 logs, the script will automatically stop.
 * -Run the script with the full inventory of logs, or either with none.
 */

public class FranjeyAIOLogsBurner extends AbstractScript{

	/*
	 * Paint variables
	 */
	private final Font font1 = new Font("Century Gothic", Font.BOLD, 13);
	private final Font font2 = new Font("Century Gothic", Font.BOLD, 0);
	private final Font font3 = new Font("Century Gothic", 0, 13);
	private Timer t = new Timer();

	/*
	 * Images
	 */
	private final Image img1 = getImage("http://i.imgur.com/fNSPWA4.png?1");

	/*
	 * Defining variables
	 */
	private Area burnArea;
	private String logsName;
	private Tile startTile;
	private Tile startTileWalk;
	private Tile newFirstStartTile;
	private Tile newSecondStartTile;
	private Tile switchTile;
	private Tile switchTileNew;
	gui gui = new gui();

	private Image getImage(String url) {
		try {
			return ImageIO.read(new URL(url));
		} catch(IOException e) {
			return null;
			
		}
	}

	/*
	 * Start
	 */
	@Override
	public void onStart() {
		getSkillTracker().start(Skill.FIREMAKING);
		getSkillTracker().start();
	}

	/*
	 * Main
	 */
	@Override
	public int onLoop() {

		if (!getClient().isLoggedIn()) {
			return 200;
		}

		getDialogues().clickContinue();

		if(!getWalking().isRunEnabled() && getWalking().getRunEnergy() > Calculations.random(30,50)){
			getWalking().toggleRun();
		}


		/*
		 * GUI
		 */
		if(getClient().isLoggedIn() && gui.start == false) {
			gui.setVisible(true);
			while (gui.isVisible()) {
				sleep(200);
			}
			logsName = gui.getLogsName();
			burnArea = gui.getBurnArea();
			startTile = gui.getStartTile();
			startTileWalk = gui.getStartTileWalk();
			newFirstStartTile = gui.getNewFirstStartTile();
			newSecondStartTile = gui.getNewSecondStartTile();
			switchTile = gui.getSwitchTile();
			switchTileNew = gui.getSwitchTileNew();
			gui.dispose();
		}
		
		
		/*
		 * Widgets
		 */
		if (getWidgets().getChildWidget(345, 0) != null
				&& getWidgets().getChildWidget(345, 0).isVisible()) {
			getWidgets().getChildWidget(345, 1).getChild(11).interact();
			log("getWidgers");
		}

		/*
		 * Banking
		 */
		
		/*
		 * -Script will stop when there are not enough logs in the bank to complete an entire inventory
		 */
		if (getBank().isOpen()) {
			if (getBank().count(logsName) < 27) {
				log("Not enough " + logsName + " in the bank. Amount is minor than 27, ending script!");
				return -1;
			}
		}	

		if (!getInventory().contains(logsName) && getPlayers().myPlayer().getAnimation() == -1) {
			log("Banking"); 
			if (!getBank().isOpen()) {
				getBank().openClosest();
				sleepUntil(new Condition() {
					public boolean verify() {
						return getBank().isOpen();
					}
				}, Calculations.random(1600, 1800));
			} else {
				getBank().withdrawAll(logsName);
				log("Withdrawing more " + logsName);
				sleepUntil(new Condition() {
					public boolean verify() {
						return getInventory().isFull();
					}
				}, Calculations.random(1600, 1800));
			}


		} 

		/*
		 * Tile switching - To burn the entire inventory
		 */
		else if (getPlayers().myPlayer().getTile().equals(switchTile)) {
			log("Walking to the new burning tile (1)");
			getWalking().walkOnScreen(newFirstStartTile);
			sleepUntil(new Condition() {
				public boolean verify() {
					return getPlayers().myPlayer().getTile().equals(newFirstStartTile);
				}
			}, Calculations.random(2800, 3000));
		}

		else if (getPlayers().myPlayer().getTile().equals(switchTileNew)) {
			log("Walking to the new burning tile (2)");
			getWalking().walkOnScreen(newSecondStartTile);
			sleepUntil(new Condition() {
				public boolean verify() {
					return getPlayers().myPlayer().getTile().equals(newSecondStartTile);
				}
			}, Calculations.random(2800, 3000));
		}

		/*
		 * Obtaining the starting burn tile and performing the burning actions
		 */
		else if (!burnArea.contains(getPlayers().myPlayer().getTile()) && getInventory().count(logsName) >= 1) {
			if(!getPlayers().myPlayer().getTile().equals(startTile) && getInventory().count(logsName) >= 27) {
				log("Walking to the burning tile");
				getWalking().walk(startTileWalk);
				getWalking().walkOnScreen(startTile);
				sleepUntil(new Condition() {
					public boolean verify() {
						return getPlayers().myPlayer().getTile().equals(startTile);
					}
				}, Calculations.random(2800, 3000));

			}
			else {
				if (getInventory().contains("Tinderbox") && getInventory().contains(logsName)){
					if (getPlayers().myPlayer().getAnimation() == -1
							&& !getPlayers().myPlayer().isMoving()) {
						log("Burning " + logsName);
						getInventory().interact("Tinderbox", "Use");
						getInventory().interact(logsName, "Use");

						sleepUntil(new Condition() {
							public boolean verify() {
								return getPlayers().myPlayer().getAnimation() != -1;
							}
						}, Calculations.random(2800, 3000));
					} 
				}
			}
		}
		return Calculations.random(500, 600);
	} 

	
	/*
	 * Paint - Graphics 2D
	 */
	public long getGainedExperience(Skill firemaking){         //gained experience
		long fm;
		fm = getSkillTracker().getGainedExperience(Skill.FIREMAKING);
		return fm;
	}

	public long getGainedExperiencePerHour(Skill firemaking){  //gained experience per hour 
		long fm;
		fm = getSkillTracker().getGainedExperiencePerHour(Skill.FIREMAKING);
		return fm;
	}

	public long getStartLevel(){  //Start level
		long fm;
		fm = getSkillTracker().getStartLevel(Skill.FIREMAKING);
		return fm;
	}

	public long getGainedLevels(){  //gained levels 
		long fm;
		fm = getSkillTracker().getGainedLevels(Skill.FIREMAKING);
		return fm;
	}

	public long getExperienceToLevel(){  //Exp to level 
		long fm;
		fm = getSkills().experienceToLevel(Skill.FIREMAKING);
		return fm;
	}

	public long getTimeToLevel() {
		long fmxp;
		long xphr;

		fmxp = getExperienceToLevel();
		xphr = getGainedExperiencePerHour(Skill.FIREMAKING);

		return fmxp/xphr;
	}

	public void onPaint(Graphics g1) {
		if(t == null){
			t = new Timer(0);
		}
		Graphics2D g = (Graphics2D)g1;
		g.drawImage(img1, 0, 307, null);
		Stroke stroke = g.getStroke();
		g.setColor(Color.BLACK);
		g.setFont(font2);
		g.setFont(font3);
		g.drawString(" " + Timer.formatTime(t.elapsed()), 122, 385);
		g.drawString(" " + getGainedExperience(Skill.FIREMAKING) + "(" + getGainedExperiencePerHour(Skill.FIREMAKING) + ")", 184, 437);
		g.drawString(" " + getStartLevel() + "(+" + getGainedLevels() + ")", 127, 410);
		g.drawString(" " + getExperienceToLevel(), 106, 463);
		g.setStroke(stroke);
	}
}		   
