This repository contains my scripts for the DreamBot platform (www,dreambot.org). You can see them under Source:

NOTE: Please do not take my code, alter it, and then rerelease it as your own. You're welcome to use snippets, with proper crediting, or take the code and alter it however you want for personal use.

My www,dreambot.org username is Fran / http://dreambot.org/forums/index.php?/user/674-franjey/

My real name is: Francisco Jurado